package salestock.test.auth;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import salestock.test.SalestockTestCase;


public class RegisterTestCase extends SalestockTestCase {
	
	@Test
	public void invalidSignUp(){
		System.out.println("Starting Test Invalid Signup");
		driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		WebElement emailInput = wait.until(ExpectedConditions.elementToBeClickable(By.id("email_create")));
		
		driver.findElement(By.id("SubmitCreate")).click();

		boolean messageExist = wait.until(ExpectedConditions.
				textToBePresentInElementLocated(By.xpath("//div[contains(@class,'alert alert-danger')]"), "Invalid email addres"));
		Assert.assertTrue("E1001: email empty", messageExist);
		
		emailInput.sendKeys("dawpio@kdowa");
		
		messageExist = wait.until(ExpectedConditions.
				textToBePresentInElementLocated(By.xpath("//div[contains(@class,'alert alert-danger')]"), "Invalid email addres"));
		Assert.assertTrue("E1003: email with random character", messageExist);
		
		emailInput.sendKeys("!@#$%^&*()-=_+[]{};':\"<>?,./");
		
		messageExist = wait.until(ExpectedConditions.
				textToBePresentInElementLocated(By.xpath("//div[contains(@class,'alert alert-danger')]"), "Invalid email addres"));
		Assert.assertTrue("E1002: email with special character", messageExist);
		
		System.out.println("End Test Invalid Signup");
	}
	
	@Test
	public void validSignUp(){
		System.out.println("Starting Test Valid Sign Up");
		driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
		
		WebDriverWait wait = new WebDriverWait(driver, 10);

		WebElement emailInput = wait.until(ExpectedConditions.elementToBeClickable(By.id("email_create")));
		
		int randomInt = (new Random()).nextInt(1000); 
		emailInput.sendKeys("rindiismail"+randomInt+"@gmail");
		
		driver.findElement(By.id("SubmitCreate")).click();
		String currentUrl = driver.getCurrentUrl();
		boolean messageExist = wait.until(ExpectedConditions.
				textToBePresentInElementLocated(By.xpath("//div[contains(@class,'alert alert-danger')]"), "Invalid email addres"));
		Assert.assertSame("E1004: valid email", "http://automationpractice.com/index.php?controller=authentication&back=my-account#account-creation", currentUrl);
		
		System.out.println("End Test Valid Sign Up");
	}
}
